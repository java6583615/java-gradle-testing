package org.example.ACM;

public class Increacing {
    public static void main(String[] args) {
        // contando sublistas crecientes

        int[] a = {5,7,2,4,6}; // resultado 3

        // ventana deslizante dinamica
        int n = a.length;
        int count = 0;
        int max = 0;
        for (int i = 0; i < n-1; i++) {
            if (a[i] < a[i+1]){
                count++;
            }else{
                max = Math.max(max, count);
                count = 0;
            }
        }
        System.out.println(count);
    }
}
