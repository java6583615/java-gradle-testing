package org.example.ACM;

import java.util.Scanner;

public class Portmanteau {
    // abcdefun -> derecha
    // ghijku <- izquierda

    public static void main(String[] args) {
        Scanner x = new Scanner(System.in);
        String s1 = x.next();
        String s2 = x.next();

        /*
        Obtenga la primera letra de la primera palabra independientemente de qué (vocal o consonante) sea. Luego, comenzando desde la
        segunda letra de la primera palabra, haga que las letras se muevan hasta llegar a una vocal. Si no hay vocales mientras se mueve
        hacia la derecha, se tomarán todas las letras. Si hay una vocal que se mueve hacia la derecha, la llamaremos v1.
         */
        String v1 = s1.charAt(0) + "";
        System.out.println("v1: " + v1);

    }

}// abcd ijku
