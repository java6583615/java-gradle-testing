package org.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class Main {
    static Scanner x = new Scanner(System.in);
    private final char[] alpahbet = "abcdefghijklmnopqrstuvwxyz".toCharArray();
    private String letter;
    private int displacement;

    public Main(String letter, int displacement){
        this.letter = letter;
        this.displacement = displacement;
    }

    public String getLetter() {
        return letter;
    }

    public int getDisplacement() {
        return displacement;
    }

    public void setLetter(String letter) {
        this.letter = letter;
    }

    public void setDisplacement(int displacement) {
        this.displacement = displacement;
    }

    public String solution(){
        String result = "";
        for (int i = 0; i < this.letter.length(); i++) {
            char currentLetter = (this.alpahbet[(this.letter.charAt(i) - 'a' + this.displacement) % 26]);
            result +=currentLetter;
        }
        return result;
    }
    public static void main(String[] args) {
        //Main res = new Main("parangaricutirimicuaro", 1);
        //System.out.println(res.solution());
        
    }

    public static void build(int limit, double[] values){
        ArrayList<Integer> s = new ArrayList<>();
        for (double value : values){
            if (value < 0.5)
                s.add(0);
            else
                s.add(1);
        }
        int N1 = getOnes(s);
        int N0 = getZero(s);
        int C0 = getRuns(s);

    }

    public static int getOnes(ArrayList<Integer> s){
        return (int) s.stream().filter(x -> x == 1).count();
    }

    public static int getZero(ArrayList<Integer> s){
        return (int) s.stream().filter(x -> x == 0).count();
    }

    public static int getRuns(ArrayList<Integer> s){
        int counter = 0;

        return s.stream().findAny().orElse(0);
    }
}
