package org.example.N_32_SphereOnlineJudge;

import java.util.Hashtable;
import java.util.Map;
import java.util.Scanner;
import java.util.Stack;

public class TransformTheExpression {
    public static String shuntingYard(String expression) {
        StringBuilder sb = new StringBuilder();

        Stack<Character> stack = new Stack<>();
        Map<Character, Integer> precedence = Map.of(
                '+', 1,
                '-', 1,
                '*', 2,
                '/', 2,
                '^', 3
        );

        for(Character i : expression.toCharArray()){
            if (isOperand(i)){
                sb.append(i);
            } else if (isOperator(i) && precedence.containsKey(i)) {
                while (!stack.isEmpty() && precedence.containsKey(stack.peek()) && precedence.get(stack.peek()) >= precedence.get(i)) {
                    sb.append(stack.pop());
                }
                stack.push(i);
            } else if (isParenthesis(i)) {
                if (i == '(') {
                    stack.push(i);
                } else {
                    while (!stack.isEmpty() && stack.peek() != '(') {
                        sb.append(stack.pop());
                    }
                    stack.pop();
                }
            }
        }

        return sb.toString();
    }

    static boolean isOperator(char c) {
        return c == '+' || c == '-' || c == '*' || c == '/' || c == '^';
    }

    static boolean isOperand(char c) {
        return c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z';
    }

    static boolean isParenthesis(char c) {
        return c == '(' || c == ')';
    }

    public static void main(String[] args) {
        Scanner x = new Scanner(System.in);
        int n = x.nextInt();

        while (n-- > 0) {
            String expression = x.next();
            System.out.println(shuntingYard(expression));
        }
    }
}
