package org.example.N_32_SphereOnlineJudge;

import java.util.Scanner;

public class TEST_LifeTheUniverseAndEverything {
    public static void main(String[] args) {
        Scanner x = new Scanner(System.in);
        while (x.hasNext()){
            int n = x.nextInt();
            if (n == 42) break;
            System.out.println(n);
        }
    }
}
