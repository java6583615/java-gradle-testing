package org.example.N_32_SphereOnlineJudge;

import java.util.*;
import java.util.stream.Stream;

public class PRIME1_PrimeGenerator {
    public static List<Integer> findPrimesInRange(int a, int b) {
        // Paso 1: Encuentra todos los primos <= √b usando la criba clásica
        int limit = (int) Math.sqrt(b);
        boolean[] isPrimeSmall = new boolean[limit + 1];
        List<Integer> primes = new ArrayList<>();

        // Inicializa todos los números como primos
        for (int i = 2; i <= limit; i++) {
            isPrimeSmall[i] = true;
        }

        // Criba clásica para marcar los números no primos
        for (int i = 2; i * i <= limit; i++) {
            if (isPrimeSmall[i]) {
                for (int j = i * i; j <= limit; j += i) {
                    isPrimeSmall[j] = false;
                }
            }
        }

        // Almacena los primos encontrados
        for (int i = 2; i <= limit; i++) {
            if (isPrimeSmall[i]) {
                primes.add(i);
            }
        }

        // Paso 2: Crea un array booleano para el rango [a, b]
        boolean[] isPrimeRange = new boolean[b - a + 1];
        Arrays.fill(isPrimeRange, true);

        // Paso 3: Marca los múltiplos de los primos en el rango [a, b]
        for (int prime : primes) {
            // Encuentra el primer múltiplo de 'prime' dentro del rango [a, b]
            int start = Math.max(prime * prime, (a + prime - 1) / prime * prime);

            for (int j = start; j <= b; j += prime) {
                isPrimeRange[j - a] = false;
            }
        }

        // Si a es 1, márquelo como no primo (1 no es primo)
        if (a == 1) {
            isPrimeRange[0] = false;
        }

        // Paso 4: Genera la lista de números primos en el rango
        List<Integer> result = new ArrayList<>();
        for (int i = 0; i < isPrimeRange.length; i++) {
            if (isPrimeRange[i]) {
                result.add(a + i);
            }
        }

        return result;
    }

    public static void main(String[] args) {
        Scanner x = new Scanner(System.in);
        int a,b;
        int n = x.nextInt();
        while(n > 0){
            a = x.nextInt();
            b = x.nextInt();
            findPrimesInRange(a, b).forEach(System.out::println);
            n--;
        }
    }
}
